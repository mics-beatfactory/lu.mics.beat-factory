
(function() {
    var reqAppControl = null;
    var shortVibTime=50;
    var longVibTime=150;
    var gString="";
    var brailleMorseTransEnum={
    		 	a:"100000",
    	        b:"110000",
    	        c:"100100",
    	        d:"100110",
    	        e:"100010",
    	        f:"110100",
    	        g:"110110",
    	        h:"110010",
    	        i:"010100",
    	        j:"010110",
    	        k:"101000",
    	        l:"111000",
    	        m:"101100",
    	        n:"10110",
    	        o:"101010",
    	        p:"111100",
    	        q:"111110",
    	        r:"111010",
    	        s:"011100",
    	        t:"011110",
    	        u:"101001",
    	        v:"111001",
    	        w:"010111",
    	        x:"101101",
    	        y:"101111",
    	        z:"101011",
    };

    function shortVibration()
    { 
       navigator.vibrate(shortVibTime);
       gString+="s";
    }
    function longVibration()
    { 
    	navigator.vibrate(longVibTime);
    	gString+="l";
    }
    function pause()
    { 
    	navigator.vibrate([10, 1000]);
    }
    function textToBrailleMorseVibration(text)
    {
    	document.getElementById("fld-text").setAttribute('value', text);
    	var strArray = text.split(" ");
    	strArray.forEach(function(word)
    	{
    		translateToVibration(word);
    	});
    }
    			
    function translateToVibration(word)
    {
    	gString+="*";
    		var letters= word.split('');
    		//for(var c in letters)
    			letters.forEach(function(letter)
    		{
    			executeVibration(letter);
    			gString+="*";
    		});
    			
    }

    function executeVibration(letter)
    {
    
    	var brailleCode = brailleMorseTransEnum[letter].split('');
    	var i;
    	for(i=0; i<6;i++){
    		if(brailleCode[i] == 1)
    			longVibration();
    		else
    			shortVibration();
    			
    	}
    		
    		
    	 }
//    function checkAppControl() {
        //try {
            // Get the requested application control passed to the current application
            //reqAppControl = tizen.application.getCurrentApplication().getRequestedAppControl();

            //if (reqAppControl && reqAppControl.appControl.operation === "http://tizen.org/appcontrol/operation/appcall") {
                // Display the information about the requested Application Control
                //document.querySelector("#callee-intro").innerHTML = "<p>This application is called by <b>" + reqAppControl.callerAppId +
                  //  "</b><br>with operation: <b>" + reqAppControl.appControl.operation.replace("http://tizen.org/appcontrol/operation/", "") + "</b>";

                //createIconTable();
                // Hide the Close button
                //document.querySelector("#btn-translate").style.display = "none";
         //   } else {
                // Display a message notifying that AppCallee only works properly when launched by AppCaller using the Application Control
               // document.querySelector("#callee-intro").innerHTML = "The application was not launched with Application Control." +
                 //   "<br><br>This application only works when it is called by another application." +
                   // "<br><br>Please launch it with the application &quot;appCaller&quot;.";

                // Hide the area for the icon table
               // document.querySelector("#icon-table").style.display = "none";
           // }
      //  } catch (error) {
        //    console.error("checkAppControl(): " + error.message);
       // }
    //}

    /**
     * Replies to the Caller application by passing data
     * @private
     * @param {Object} event - the object for click event
     */
    function replyToCaller(event) {
        var data = null,
            imgId = event.target.id;

        try {
            // Define the data in a key/value-pair format to be passed through the Application Control interface
            // The value must be a DOMString array
            data = new tizen.ApplicationControlData("text", [imgId]);

            // Pass the data to the Caller
            // The data must be an Object array
            reqAppControl.replyResult([data]);

            // Close the current application (AppCallee)
            tizen.application.getCurrentApplication().exit();
        } catch (e) {
            alert("Return failed. \n reason: " + e.message);
            console.error("return failed. reason: " + e.message);
        }
    }

    /**
     * Creates the table of the icon images
     * @private
     */
//    function createIconTable() {
//        var i,
//            svgImgTag = "",
//            svgIcon = [],
//            ICON_N = 16;
//
//        for (i = 1; i <= ICON_N; i++) {
//            svgImgTag += "<img class='svg-icon' id='" + i + "' src='./image/" + i + ".svg'/>";
//        }

//        document.querySelector("#icon-table").innerHTML = "<p>Please choose an icon.</p>" + svgImgTag;
//
//        svgIcon = document.querySelectorAll(".svg-icon");
//
//        // Add event listener for every icon so that when it is clicked, it will reply to the Caller
//        for (i = 0; i < svgIcon.length; i++) {
//            svgIcon[i].addEventListener("click", replyToCaller);
//        }
//    }


    /**
     * Sets default event listeners
     * @private
     */
    function setDefaultEvents() {
        document.getElementById("btn-translate").addEventListener("click", function() {
            try {
            	gString="";
            	var text=document.getElementById("fld-text").value;
            	textToBrailleMorseVibration(text);
            	
            	console.log(gString);
                //tizen.application.getCurrentApplication().exit();
            } catch (error) {
                console.error("getCurrentApplication(): " + error.message);
            }
        });

        // Add eventListener for tizenhwkey
        document.addEventListener("tizenhwkey", function(e) {
            if (e.keyName === "back") {
                try {
                    tizen.application.getCurrentApplication().exit();
                } catch (error) {
                    console.error("getCurrentApplication(): " + error.message);
                }
            }
        });
    }

    /**
     * Initiates the application
     * @private
     */
    function init() {
        setDefaultEvents();

       // checkAppControl();
    }

    window.onload = init;
}());